package com.partner.wechat.message.resp;

/**
 * 包      名：  com.partner.wechat.message.resp
 * 创 建 人：   寻欢
 * 创建时间：  2016/9/19 15:54
 * 修 改 人：
 * 修改日期：
 *
 * 图片消息
 */
public class ImageMessage {
	private Image Image;

	public Image getImage() {
		return Image;
	}

	public void setImage(Image image) {
		Image = image;
	}
}
